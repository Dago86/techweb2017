''' http://site1719.tw.cs.unibo.it/Esercitazione02_Python.pdf
*** Esercizio 4
Definire due funzioni chiamate:
•
is_greater_than_zero
, che restituisce un booleano se tutti i numeri specificati come input 
sono maggiori di zero, e
•
divide_all
, che divide due o più numeri, e restituisce il risultato di divisioni successive tra 
tutti questi nell'ordine seguente
(((n1 / n2) / n3 ) / n4) / ,,,
Scrivere uno script Python che chieda all'utente una serie di numeri separati da spazio e, usando le 
funzioni introdotte sopra, se tutti i numeri sono maggiori di zero, allora li divide tutti. Bisogna 
gestire appropriatamente (ad esempio con le eccezioni) il caso in cui l'utente non specifichi almeno 
due numeri. Il risultato deve essere stampato a video.  *** '''




'''Per ora manca eccezione di controllo di minimo ai due numeri'''

def greater_than_zero(num):
    for x in range(0, len(num)):
        if (num[x]<= 0):
        	numx=num[x]
	        print("Minore di zero", numx)
	        return False
    return True


def divide_all(num):
    risultato = num[0]
    for x in range(1, len(num)):
        try:
 	        risultato = risultato /(num[x])
 	except ZeroDivisionError :
 		print("Molto male, hai tentato una divisione per zero")
 	    	risultato = 0
    return risultato





num = []
n = int(raw_input("Quanti valori vuoi inserire?"))
for i in xrange(0, n):
    num.append(float(raw_input('Inserisci il prossimo valore ')))
print num
if greater_than_zero(num) == False:
	print("greater_than_zero: hai inserito un valore minore di zero")
da = divide_all(num)
print ("Il risultato di divide_all: ", da)